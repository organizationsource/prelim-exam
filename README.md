# Prelim-Exam
## Instructions

1. Present your answer during coding session, explain the flow, display all related stuffs except the specific line of code which is the Fetching API method.
2. Screenshot HelloWorld.vue and send directly to Miss Jo via PM 
3. Dont push directly the codes, make sure, each contributor must create a branch before pushing it to the repository, indicate it with your fullname. Remove first the line of code that has fetching API method. 
4. Lastly, double check your code, make sure it is working and you will get the perfect score on or before March 29, 2021.