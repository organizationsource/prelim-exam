import { createRouter, createWebHistory } from '@ionic/vue-router';
import { RouteRecordRaw } from 'vue-router';
import HelloWorld from '../views/HelloWorld.vue'
import Home from '../views/Home.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: '/hello-world'
  },
  {
    path: '/hello-world',
    name: 'HelloWorld',
    component: HelloWorld
  },
  {
     path: '/home',
     name: 'Home',
     component: Home
   }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
